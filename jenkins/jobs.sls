include:
  - jenkins
  - jenkins.cli

{% from "jenkins/map.jinja" import jenkins with context %}
{% from 'jenkins/macros.jinja' import jenkins_cli with context %}

jenkins_install_pip:
  pkg.installed:
    - name: {{ jenkins.pip_package }}

jenkins_install_git:
  pkg.installed:
    - name: git

install_jenkins_job_builder:
  pip.installed:
    - name: jenkins-job-builder=={{ jenkins.job_builder_version }}
    - require:
      - pkg: jenkins_install_pip

deploy_jenkins_job_builder_config:
  file.managed:
    - name: {{ jenkins.home }}/jenkins_jobs.ini
    - source: salt://jenkins/files/jenkins_jobs.ini.j2
    - template: jinja

{% if jenkins.job_builder_secret is defined %}
{% from 'jenkins/macros.jinja' import jenkins_cli with context %}
deploy_jenkins_job_builder_key_xml:
  file.managed:
    - name: {{ jenkins.home }}/job_builder_secret_key
    - source: salt://jenkins/files/job_builder_secret_key.j2
    - template: jinja

deploy_jenkins_job_builder_key:
  cmd.run:
    - name: >-
        cat {{ jenkins.home }}/job_builder_secret_key |
        {{ jenkins_cli('create-credentials-by-xml') }}
        "SystemCredentialsProvider::SystemContextResolver::jenkins" "(global)"
    - unless: >-
        {{ jenkins_cli('list-credentials') }}
        "SystemCredentialsProvider::SystemContextResolver::jenkins"
        |grep {{ jenkins.job_builder_secret_id }}
    - require:
      - cmd: jenkins_responding
{% endif %}

deploy_jenkins_job_builder_job_config:
  file.managed:
    - name: {{ jenkins.home }}/jenkins_job_builder_job.yml
    - source: salt://jenkins/files/jenkins_job_builder_job.yml.j2
    - template: jinja

deploy_jenkins_job_builder_job:
  # We want this to run only when the deploy_jenkins_job_builder_job_config
  # resulted in a change but there doesnt seem to be an easy way to achieve that
  cmd.run:
    - name: >
        jenkins-jobs
        --conf {{ jenkins.home }}/jenkins_jobs.ini
        update {{ jenkins.home }}/jenkins_job_builder_job.yml
    - require:
        - file: deploy_jenkins_job_builder_config
        - file: deploy_jenkins_job_builder_job_config
        - pip: install_jenkins_job_builder
        - cmd: jenkins_responding
