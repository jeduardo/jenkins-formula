# Jenkins

## Usage

### Available states

* `jenkins`: Installs and configures jenkins
* `jenkins/plugins`: Installs Jenkins plugins
* `jenkins/jobs`: Install Jenkins jobs

## Development

Requirements:

* Docker
* Kitchen: `bundle install`

* Install dependencies: `make vendor`
* Use kitchen: `kitchen create && kitchen converge && kitchen verify`
